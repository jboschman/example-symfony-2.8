<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));
    }

    /**
     * @Route("/add", name="add")
     *
     * @param Request $request
     * @return Response
     */
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $post = new Post();
        $post->setName(uniqid());
        $post->setAdded(new \DateTime('now'));

        $em->persist($post);
        $em->flush();

        return $this->render('default/add.html.twig', ['postId' => $post->getId()]);
    }

    /**
     * @Route("/show", name="show")
     *
     * @param Request $request
     * @return Response
     */
    public function showAction(Request $request)
    {
        $posts = $this->getDoctrine()
            ->getRepository(Post::class)
            ->findAll();
        return $this->render('default/show.html.twig', ['posts' => $posts]);
    }

    /**
     * @Route("/mail", name="mail")
     *
     * @param Request $request
     * @return Response
     */
    public function mailAction(Request $request)
    {
        $posts = $this->getDoctrine()
            ->getRepository(Post::class)
            ->findAll();
        $from = $this->container->getParameter('from_email');
        $to = $this->container->getParameter('to_email');


        $message = (new \Swift_Message('Test Email'))
            ->setFrom($from)
            ->setTo($to)
            ->setBody(
                $this->renderView(
                    'email/test.txt.twig',
                    [
                        'posts'         => $posts,
                    ]
                ),
                'text/plain'
            )
        ;

        $this->get('mailer')->send($message);

        return $this->render('default/mail.html.twig', ['email_address' => $to]);
    }

}
